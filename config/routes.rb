Rails.application.routes.draw do

  Rails.application.routes.draw do
   resources :quiz
   root 'quiz#top' #トップページをblogsコントローラのindexアクションに設定
  end

  get 'quiz/photo' => 'quiz/photo'
  post 'quiz/answer'=> 'quiz#answer'
  get '/' => 'quiz#top'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
