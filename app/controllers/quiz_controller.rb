require 'net/http'
require 'uri'
require 'json'

class QuizController < ApplicationController

  def top
    @actress=Actress.new
  end

  def answer
      #curlコマンドを送信して、機械学習APIにPOSTを送信
      uri = URI.parse("https://intense-lake-62391.herokuapp.com/predict")
      request = Net::HTTP::Post.new(uri)
      request.content_type = "application/json"
      request.body = JSON.dump({
        "feature" => [
          params[:level].to_i,
          params[:age].to_i,
          params[:big].to_i,
          params[:old].to_i,
          params[:nobitch].to_i
        ]
      })

      req_options = {
        use_ssl: uri.scheme == "https",
      }

      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        http.request(request)
      end

      #JSONから回答部分を抽出・Integerに変換
      actress=response.body[49].to_i
      session[:answer]=actress

      #回答に応じてリンク先を分岐させる
      if actress==0
        redirect_to("https://ja.wikipedia.org/wiki/%E6%98%8E%E6%97%A5%E8%8A%B1%E3%82%AD%E3%83%A9%E3%83%A9")
      elsif actress==1
        redirect_to("https://ja.wikipedia.org/wiki/%E7%B4%97%E5%80%89%E3%81%BE%E3%81%AA")
      elsif actress==2
        redirect_to("https://ja.wikipedia.org/wiki/%E4%B8%89%E4%B8%8A%E6%82%A0%E4%BA%9C")
      elsif actress==3
        redirect_to("https://ja.wikipedia.org/wiki/AIKA_(AV%E5%A5%B3%E5%84%AA)")
      else
        render("/quiz/top")
      end

  end

  def photo

  end

end
